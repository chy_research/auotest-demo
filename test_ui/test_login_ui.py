# -*- coding: utf-8 -*-
# @Time    : 2021/8/10 10:49
# @Author  : Flora.Chen
# @File    : test_login_ui.py
# @Software: PyCharm
# @Desc: 登录的测试用例
import time

import allure
import pytest
from page.login_page import LoginPage
from page.users_center_page import UsersCenterPage
from data.ui_data import login_data


@pytest.mark.ui
@pytest.mark.forge
@allure.feature("登录模块-UI")
class TestLoginUi:
    """
    登录的UI测试用例
    """

    @allure.story("正确用户名和密码登录成功")
    @pytest.mark.parametrize("case", login_data.success)
    def test_login_success(self, init_driver, case, env, get_user):
        """
        名称：正确用户名和密码登录成功
        步骤：
        1. 打开浏览器
        2. 进入登录页面
        3. 输入用户名和密码
        4. 点击登录按钮
        断言：出现用户昵称以及浏览器地址正确
        """
        allure.step(case.get("title"))
        # 从配置文件中获取对应的用户信息
        if case["user"] == "{}":
            res = case["user"].format(get_user["user"])
            case["user"] = res
        if case["pwd"] == "{}":
            res = case["pwd"].format(get_user["pwd"])
            case["pwd"] = res
        if case["expected"] == "{}":
            res = case["expected"].format(get_user["nickname"])
            case["expected"] = res

        host = env.get("forge_env")
        init_driver.delete_all_cookies()
        login_page = LoginPage(driver=init_driver, host=host)
        users_page = UsersCenterPage(driver=init_driver, login_name=case["user"], host=host)
        with allure.step("在登录弹窗中输入用户名:[{}]和密码:[{}]，点击登录".format(case["user"], case["pwd"])):
            login_page.load().login(case["user"], case["pwd"])
        time.sleep(5)
        actual = users_page.get_user()
        assert init_driver.current_url == users_page.url
        assert actual == case["expected"]

    @allure.story("登录失败-错误提示是输入框下红色提示")
    @pytest.mark.skip("这个错误提示无法重现，暂时忽略")
    @pytest.mark.parametrize("case", login_data.failed_error_message)
    def test_login_wrong_error_message(self, init_driver, case, env, get_user):
        """
        名称：登录失败-错误提示是输入框下红色提示
        步骤：
        1. 打开浏览器
        2. 进入登录页面
        3. 输入不正确的用户名和密码
        4. 点击登录按钮
        断言：输入框下出现红色测试提示信息
        """
        allure.step(case.get("title"))
        # 从配置文件中获取对应的用户信息
        if case["user"] == "{}":
            res = case["user"].format(get_user["user"])
            case["user"] = res
        if case["pwd"] == "{}":
            res = case["pwd"].format(get_user["pwd"])
            case["pwd"] = res
        if case["expected"] == "{}":
            res = case["expected"].format(get_user["nickname"])
            case["expected"] = res

        host = env.get("forge_env")
        init_driver.delete_all_cookies()
        login_page = LoginPage(driver=init_driver, host=host)
        login_page.load().login(case["user"], case["pwd"])
        actual = login_page.error_message()
        time.sleep(3)
        for index, elem in enumerate(actual):
            assert case["expected"][index] in elem

    @allure.story("登录失败，错误提示是弹窗")
    @pytest.mark.skip("密码错误会限制60分钟内不能登录")
    @pytest.mark.parametrize("case", login_data.failed_error_pop)
    def test_login_wrong_error_pop(self, init_driver, case, env, get_user):
        """
        名称：登录失败，错误提示是弹窗
        步骤：
        1. 打开浏览器
        2. 进入登录页面
        3. 输入正确的用户名和错误的密码
        4. 点击登录按钮
        断言：右上角出现弹框错误提示
        """
        allure.step(case.get("title"))
        # 从配置文件中获取对应的用户信息
        if case["user"] == "{}":
            res = case["user"].format(get_user["user"])
            case["user"] = res
        if case["pwd"] == "{}":
            res = case["pwd"].format(get_user["pwd"])
            case["pwd"] = res
        if case["expected"] == "{}":
            res = case["expected"].format(get_user["nickname"])
            case["expected"] = res

        host = env.get("forge_env")
        init_driver.delete_all_cookies()
        login_page = LoginPage(driver=init_driver, host=host)
        login_page.load().login(case["user"], case["pwd"])
        actual = login_page.error_pop()
        time.sleep(3)
        for index, elem in enumerate(actual):
            assert case["expected"][index] in elem

    @allure.story("密码为空无法点击登录按钮")
    @pytest.mark.parametrize("case", login_data.failed_password_empty)
    def test_login_wrong_password_empty(self, init_driver, case, env, get_user):
        """
        名称：密码为空无法点击登录按钮
         步骤：
        1. 打开浏览器
        2. 进入登录页面
        3. 输入正确的用户名，不输入密码
        断言：登录按钮置灰，无法点击
        """
        allure.step(case.get("title"))
        # 从配置文件中获取对应的用户信息
        if case["user"] == "{}":
            res = case["user"].format(get_user["user"])
            case["user"] = res
        if case["pwd"] == "{}":
            res = case["pwd"].format(get_user["pwd"])
            case["pwd"] = res
        if case["expected"] == "{}":
            res = case["expected"].format(get_user["nickname"])
            case["expected"] = res

        host = env.get("forge_env")
        init_driver.delete_all_cookies()
        login_page = LoginPage(driver=init_driver, host=host)
        with allure.step("在登录弹窗中输入用户名：[{}]和密码:[{}]".format(case["user"], case["pwd"])):
            login_page.load().input_login_info(case["user"], case["pwd"])
        actual = login_page.login_button_class()
        time.sleep(3)
        assert actual == case["expected"]
