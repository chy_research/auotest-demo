# -*- coding: utf-8 -*-
# @Time    : 2021/8/13 20:40
# @Author  : Flora.Chen
# @File    : draft_bk.py.py
# @Software: PyCharm
# @Desc:
import os.path
from datetime import datetime
import allure
import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as CH_Options
from selenium.webdriver.firefox.options import Options as FF_Options
from config.settings import RunConfig
from config.settings import IMG_DIR
from loguru import logger
from page.login_page import LoginPage


# 截图
def capture_screenshots(filename):
    """
    配置用例失败截图路径
    :param filename: 文件名
    :return:
    """
    global driver
    path = os.path.join(IMG_DIR, filename)
    RunConfig.driver.save_screenshot(path)


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport():
    """
    allure-pytest的报错截图添加到报告
    :param item:
    """
    # 获取常规钩子方法的调用结果,返回一个result对象
    outcome = yield
    # # 获取调用结果的测试报告，返回一个report对象
    # report对象的属性包括when（steup, call, teardown三个值）、nodeid(测试用例的名字)、outcome(用例的执行结果，passed,failed)
    report = outcome.get_result()
    if report.when == "call" or report.when == "setup":
        xfail = hasattr(report, "wasxfail")
        if (report.skipped and xfail) or (report.failed and not xfail):
            file_name = report.nodeid.split("::")[-1] + "_" + datetime.now().strftime("%Y-%m-%d %H_%M_%S") + ".png"
            capture_screenshots(file_name)
            img_path = os.path.join(IMG_DIR, file_name)
            if img_path:
                with allure.step("点击查看失败截图..."):
                    allure.attach.file(source=img_path, name=report.nodeid, attachment_type=allure.attachment_type.PNG)


# 启动浏览器
@pytest.fixture(scope="module", autouse=True)
def init_driver():
    """
    定义浏览器驱动
    :return:
    """
    # 可以不指定路径
    # driver_path = RunConfig.driver_path
    # logger.debug(f"获取浏览器驱动位置：{driver_path}")
    if RunConfig.driver_type == "chrome":
        # 本地chrome浏览器
        logger.debug("运行的浏览器：chrome")
        driver = webdriver.Chrome()
        # driver = webdriver.Chrome(driver_path)
        driver.maximize_window()
        driver.implicitly_wait(10)
        driver.maximize_window()
        driver.delete_all_cookies()  # 清除浏览器所有缓存

    elif RunConfig.driver_type == "firefox":
        # 本地firefox浏览器
        logger.debug("运行的浏览器：firefox")
        driver = webdriver.Firefox()
        driver.maximize_window()
        driver.implicitly_wait(10)
        driver.maximize_window()
        driver.delete_all_cookies()  # 清除浏览器所有缓存

    elif RunConfig.driver_type == "chrome-headless":
        # chrome headless模式
        logger.debug("运行的浏览器：chrome-headless")
        # 参数说明，参考地址：https://github.com/GoogleChrome/chrome-launcher/blob/master/docs/chrome-flags-for-tools.md#--enable-automation
        chrome_option = CH_Options()
        # chrome_option = webdriver.ChromeOptions()
        chrome_option.add_argument("--headless")
        chrome_option.add_argument("--no-sandbox")  # 注意：linux运行必须要有这个
        chrome_option.add_argument("--window-size=1920x1080")
        driver = webdriver.Chrome(options=chrome_option)
        # driver = webdriver.Chrome(executable_path=driver_path, options=chrome_option)
        driver.implicitly_wait(10)
        driver.delete_all_cookies()  # 清除浏览器所有缓存

    elif RunConfig.driver_type == "firefox-headless":
        # firefox headless模式
        logger.debug("运行的浏览器：firefox-headless")
        firefox_options = FF_Options()
        firefox_options.headless = True
        driver = webdriver.Firefox(firefox_options=firefox_options)
        driver.implicitly_wait(10)
        driver.delete_all_cookies()  # 清除浏览器所有缓存

    else:
        raise NameError("driver驱动类型定义错误！")
    RunConfig.driver = driver
    yield driver
    # 关闭浏览器
    driver.quit()


@pytest.fixture(scope="module", autouse=True)
def login_driver(get_user, env, init_driver):
    login_page = LoginPage(host=env.get("forge_env"), driver=init_driver)
    login_page.load().login(username=get_user.get("user"), password=get_user.get("pwd"))
    RunConfig.driver = init_driver
    yield init_driver
    init_driver.quit()


