# -*- coding: utf-8 -*-
# @Time    : 2021/8/20 14:05
# @Author  : Flora.Chen
# @File    : test_new_project.py
# @Software: PyCharm
# @Desc:
import time

import allure
import pytest
from page.users_center_page import UsersCenterPage
from data.ui_data import new_project_data
from page.project_detail_page import ProjectDetailPage



@pytest.mark.ui
@pytest.mark.debug
@allure.feature("项目模块-UI")
class TestNewProject:
    """新建项目的测试用例"""

    @allure.story("新建项目")
    @pytest.mark.parametrize("case", new_project_data.new_project)
    def test_new_project(self, login_driver, case, get_user, env):
        """新建项目的测试用例"""
        allure.step(case.get("title"))
        loginname = get_user.get("user")
        host = env.get("forge_env")
        repository_name = case.get("repository_name")
        # 初始化页面
        users_page = UsersCenterPage(driver=login_driver, host=host, login_name=loginname)
        project_page = ProjectDetailPage(driver=login_driver, login_name=loginname, host=host,
                                         repository_name=repository_name)
        # 点击新建图标
        users_page.click_new_icon()
        # 新建项目
        users_page.new_project(**case)
        # 断言
        assert login_driver.current_url == project_page.url
        if case.get("private"):
            assert project_page.get_project_private_tag()

        # 删除项目
        project_page.delete_project()
        assert project_page.delete_project_success()

    @allure.story("导入项目")
    @pytest.mark.parametrize("case", new_project_data.export_project)
    def test_export_project(self, login_driver, case, get_user, env):
        """导入项目的测试用例"""
        allure.step(case.get("title"))
        loginname = get_user.get("user")
        host = env.get("forge_env")
        repository_name = case.get("repository_name")
        # 初始化页面
        users_page = UsersCenterPage(driver=login_driver, host=host, login_name=loginname)
        project_page = ProjectDetailPage(driver=login_driver, login_name=loginname, host=host,
                                         repository_name=repository_name)
        # 点击新建图标
        users_page.click_new_icon()
        # 导入项目
        users_page.export_project(**case)
        # 断言
        assert login_driver.current_url == project_page.url
        if case.get("private"):
            assert project_page.get_project_private_tag()

        if case.get("mirror_type"):
            assert project_page.get_sync_mirror_button()

        time.sleep(10)

        # 删除项目
        project_page.delete_project()
        assert project_page.delete_project_success()


