# auotest
python+request/selenium+pytest_allure集成的API &amp; UI自动化测试框架

# 项目结构
├────.gitignore git上传时忽略的文件
├────api/ 存放项目的基础API信息
│    ├────__init__.py
│    ├────login_api.py 登录接口
│    └────project_api.py 项目接口
├────common/ 存放公共方法
│    ├────__init__.py
│    ├────baseapi.py 封装基础的request方法
│    ├────basepage.py 封装浏览器基础的操作方法，用于UI自动化测试
│    ├────download_img.py 封装通过URL下载图片的方法
│    ├────handle_email.py python自带的SMTP模块发送邮件 （目前项目未使用该模块）
│    ├────handle_excel.py 使用openpyxl对excel进行读写操作
│    ├────handle_faker.py 使用faker模块造测试数据
│    ├────handle_log.py python自带的logging模块对日志进行处理（目前项目未使用该模块）
│    ├────handle_mysql.py 使用pymysql对mysql数据库进行操作
│    ├────handle_platform.py 跨平台的支持allure，用于生成allure测试报告
│    ├────handle_time.py 处理时间的类
│    ├────handle_yagmail.py 使用yagmail对自动发送报告邮件
│    ├────handle_yaml.py 使用pyyaml对yaml文件的读写方法
│    ├────helper.py 封装一些小方法
│    └────project_tree.py 获取项目文件树的方法
├────config/ 存放环境配置信息
│    ├────__init__.py
│    ├────report_template.html 直接通过python发送邮件的内容正文模板
│    ├────report_template_jenkins.html 通过jenkins发送邮件的内容正文模板
│    ├────settings.py 项目配置文件
│    └────settings_example.py 项目配置示例
├────data/ 存放测试数据
│    ├────api_data/ 存放API测试的测试数据
│    │    └────login_data.yaml 登录用例的测试数据
│    └────ui_data/ 存放UI测试的测试数据
│    │    ├────login_data.py 登录用例的测试数据
│    │    └────new_project_data.py 新建项目的测试数据
├────page/  封装页面元素定位和操作，用于UI自动化测试
│    ├────__init__.py
│    ├────login_page.py 登录页面的元素定位和操作
│    ├────project_detail_page.py 项目详情页的元素定位和操作
│    └────users_center_page.py 个人主页的元素定位和操作
├────tools/
│    ├────__init__.py
│    └────export_issue.py
├────lib/ 存放第三方库
│    └────__init__.py
├────log/  存放日志文件
├────test_api/ 存放api测试用例
│    ├────__init__.py
│    └────conftest.py 作用于当前包的fixture的配置文件
├────test_app/ 存放app测试用例
│    ├────__init__.py
│    └────conftest.py 作用于当前包的fixture的配置文件
├────test_ui/ 存放UI测试用例
│    ├────__init__.py
│    └────conftest.py 作用于当前包的fixture的配置文件
├────report/ 存放allure测试报告
├────image/ 存放UI测试过程中的截图
├────conftest.py 作用于整个项目的全局的fixture的配置文件
├────Pipfile  记录虚拟环境的相关信息
├────Pipfile.lock 记录了当前虚拟环境中安装的依赖的版本号以及哈希
├────pytest.ini pytest的配置文件
├────README.md
├────requirements.txt 项目依赖包，使用了pipenv后，该文件不需要
└────run.py 项目的运行文件


# pipenv使用
# 参考连接：https://zhuanlan.zhihu.com/p/71598248
安装pipenv: pip3 install pipenv
创建虚拟环境：pipenv install
激活已存在的虚拟环境（如果不存在会创建一个）：pipenv shell

#注意：以下三个参数只能单独使用。它们还具有破坏性，会删除当前的虚拟环境，然后用适当版本的虚拟环境替代。
指定使用Python3.6的虚拟环境: pipenv --python 3.6 
使用系统的Python2在创建虚拟环境: pipenv --two        
使用系统的Python3在创建虚拟环境: pipenv --three   

列出本地工程路径: pipenv --where
列出虚拟环境路径：pipenv --venv
列出虚拟环境python的可执行路径：pipenv --py
安装包：pipenv isntall 包名
安装包到开发环境：pipenv install 包名--dev
更新包：pipenv update 包名
卸载包：pipenv uninstall 包名
卸载所有包：pipenv uninstall --all
查看包依赖：pipenv graph
更新Pipfile.lock文件锁定当前环境的依赖版本：pipenv lock
运行py文件：pipenv run python "python文件.py"
删除虚拟环境：pipenv --rm
退出虚拟环境：exit

# 命令参考
更新依赖包：pipreqs . --encoding=utf8 --force
安装依赖包：pipenv install -r requirements.txt
jenkins工作目录：/var/lib/jenkins/workspace
