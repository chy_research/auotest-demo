# -*- coding: utf-8 -*-
# @Time    : 2021/8/30 10:34
# @Author  : Flora.Chen
# @File    : project_api.py
# @Software: PyCharm
# @Desc: 项目接口

from common.baseapi import BaseApi
from api.login_api import TrustieLogin
from loguru import logger


class ProjectApi(BaseApi):
    """项目相关的api"""

    def __init__(self, host):
        super().__init__()
        self.host = host

    def delete_project(self, **kwargs):
        """删除项目"""
        url = "/api/" + str(kwargs.get("owner")) + "/" + str(kwargs.get("repository_name")) + ".json"
        response = self.send_request(url=self.host + url, method="DELETE", cookies=kwargs.get("cookies"))
        try:
            json_data = response.json()
            if json_data["message"] == "success" and json_data["status"] == 0:
                logger.debug('删除项目接口请求成功！')
                return response
        except Exception as e:
            logger.error(f"删除项目接口请求错误：{e}")


