# -*- coding: utf-8 -*-
# @Time    : 2021/8/10 10:15
# @Author  : Flora.Chen
# @File    : login_page.py
# @Software: PyCharm
# @Desc: 登录页面的元素定位 和 操作
import time
from common.basepage import BasePage
from selenium.webdriver.common.by import By
from config.settings import IMG_DIR

# ------------------------------ 登录元素定位 ---------------------------------------#

# 首页 登录按钮
login_button = (By.XPATH, "//a[text()='登录']")
# 弹框中的用户名输入框
username_inputbox = (By.XPATH, "//input[@name='username']")
# 弹框中的密码输入框
password_inputbox = (By.XPATH, "//input[@name='password']")
# 弹框中的登录按钮
login_button_on_pop = (By.XPATH, "//div[text()='登录']")
# 弹框的错误提示信息
error_pop = (By.XPATH, "//div[@class='ant-notification-notice-description']")
# 输入框下标红的错误提示信息
error_message = (By.ID, "username_error_notice")


# ------------------------------ 登录 操作 ---------------------------------------#
class LoginPage(BasePage):
    """
    登录页面的一系列操作
    """
    def __init__(self, driver, host):
        self.host = host
        self.driver = driver

    def load(self):
        self.visit(self.host)
        return self

    def input_login_info(self, username, password):
        self.wait_element_clickable(login_button).click()
        self.wait_element_visibility(username_inputbox).send_keys(username)
        self.wait_element_visibility(password_inputbox).send_keys(password)
        time.sleep(5)
        return self

    def login(self, username, password):
        self.wait_element_clickable(login_button).click()
        self.wait_element_visibility(username_inputbox).send_keys(username)
        self.wait_element_visibility(password_inputbox).send_keys(password)
        time.sleep(5)
        self.wait_element_visibility(login_button_on_pop).click()
        time.sleep(5)
        return self

    def error_pop(self):
        """弹框的错误提示"""
        elems = self.driver.find_elements(*error_pop)
        return [elem.text for elem in elems]

    def error_message(self):
        """输入框标红的错误提示"""
        elems = self.driver.find_elements(*error_message)
        return [elem.text for elem in elems]

    def login_button_class(self):
        """获取弹框中登录按钮的class属性"""
        return self.get_class(login_button_on_pop)

