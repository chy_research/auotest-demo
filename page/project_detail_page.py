# -*- coding: utf-8 -*-
# @Time    : 2021/8/24 10:09
# @Author  : Flora.Chen
# @File    : project_detail_page.py
# @Software: PyCharm
# @Desc: 项目详情页
import time

from common.basepage import BasePage
from selenium.webdriver.common.by import By

# ------------------------------项目详情页 元素定位---------------------------------------#
# 私有仓库标识图标
project_status_tag = (By.XPATH, "//span[contains(@class, 'privateTag')]")
# 同步镜像按钮
sync_mirror_button = (By.XPATH, "//a[text()='同步镜像']")
# 仓库设置tab
repository_settings = (By.XPATH, "//span[text()='仓库设置']")
# 删除仓库按钮
delete_repository_button = (By.XPATH, "//a[text()='删除本仓库']")
# 确定按钮
confirm_button = (By.XPATH, "//a[text()='确定']")
# 仓库删除成功提示语
delete_repository_success = (By.XPATH, "//div[text()='仓库删除成功！']")


# ------------------------------项目详情页 操作---------------------------------------#
class ProjectDetailPage(BasePage):
    """
    项目详情页
    """

    def __init__(self, driver, login_name=None, host=None, repository_name=None):
        self.host = host
        self.driver = driver
        self.url = self.host + "/" + str(login_name) + "/" + str(repository_name)

    def load(self):
        self.visit(self.url)
        return self

    def get_project_private_tag(self):
        """获取项目的状态标签（私有项目会有私有tag）"""
        return self.find_elements(project_status_tag)

    def get_sync_mirror_button(self):
        """同步镜像按钮是否存在"""
        return self.find_elements(sync_mirror_button)

    def delete_project(self):
        """删除仓库"""
        time.sleep(2)
        # 在仓库详情页点击仓库设置
        self.hit(repository_settings)
        time.sleep(2)
        # 往下滑动
        js = "window.scrollTo(0, 440)"
        self.execute_js(js)
        # 点击删除仓库按钮
        self.hit(delete_repository_button)
        # 点击确定按钮
        self.hit(confirm_button)
        return self

    def delete_project_success(self):
        """仓库删除成功提示语"""
        return self.find_elements(delete_repository_success)
