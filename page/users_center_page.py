# -*- coding: utf-8 -*-
# @Time    : 2021/8/10 10:50
# @Author  : Flora.Chen
# @File    : users_center_page.py
# @Software: PyCharm
# @Desc: 个人主页 的元素定位 和 操作
import time
from common.basepage import BasePage
from selenium.webdriver.common.by import By

# ------------------------------个人主页 元素定位---------------------------------------#


# 左侧 用户头像下的 用户昵称
user_nickname = (By.XPATH, "//div[@class='userDescription']/preceding-sibling::div")
# 右上角新建项目/导入项目/新建组织/加入项目的图标
new_project_icon = (By.XPATH, "//i[contains(@class, 'icon-tianjiafangda')]")
# 新建项目
create_project = (By.XPATH, "//a[text()='新建项目']")
# 导入项目
export_project = (By.XPATH, "//a[text()='导入项目']")
# 新建组织
create_organization = (By.XPATH, "//a[text()='新建组织']")
# 加入项目
join_project = (By.XPATH, "//a[text()='加入项目']")

# ------------------------------新建镜像/托管项目---------------------------------------#


# 导入仓库URL
export_url = (By.XPATH, "//input[@id='NewWorkForm_clone_addr']")
# 需要授权验证
authorization_button = (By.XPATH, "//p[contains(text(), '需要授权验证')]")
# 镜像仓库来源的用户名
mirror_username = (By.XPATH, "//input[@id='NewWorkForm_auth_username']")
# 镜像仓库来源的密码
mirror_password = (By.XPATH, "//input[@id='NewWorkForm_password']")

# 拥有者输入框
owner_inpubox = (By.XPATH, "//label[@title='拥有者']/../following-sibling::div//input")
# 项目名称
project_name = (By.XPATH, "//input[@id='NewWorkForm_name']")
# 项目标识
depository_name = (By.XPATH, "//input[@id='NewWorkForm_repository_name']")
# 项目简介
project_desc = (By.XPATH, "//textarea[@id='NewWorkForm_description']")

# .gitignore checkbox
gitignore_checkbox = (By.XPATH, "//input[@id='NewWorkForm_ignoreFlag']")
# .gitignore 输入框
gitignore_selectbox = (By.XPATH, "//div[@id='NewWorkForm_ignore']")
# .gitignore-下拉框值
gitignore_value = (By.XPATH, "//li[text()='{}']")

# 开源许可证 checkbox
certificate_checkbox = (By.XPATH, "//input[@id='NewWorkForm_licenseFlag']")
# 开源许可证 输入框
certificate_selectbox = (By.XPATH, "//div[contains(text(), '请选择开源许可证')]")
# 开源许可证-下拉框值
certificate_value = (By.XPATH, "//li[text()='{}']")

# 将项目设置为私有-默认是公有
is_private_checkbox = (By.XPATH, "//input[@id='NewWorkForm_private']")

# 项目类别 checkbox
project_type_checkbox = (By.XPATH, "//input[@id='NewWorkForm_categoreFlag']")
# 项目类别 输入框
project_type_selectbox = (By.XPATH, "//div[text()='请选择项目类别']")
# 项目类别-下拉框值
project_type_value = (By.XPATH, "//li[text()='{}']")

# 项目语言 checkbox
project_language_checkbox = (By.XPATH, "//input[@id='NewWorkForm_languageFlag']")
# 项目类别 输入框
project_language_selectbox = (By.XPATH, "//div[text()='请选择项目语言']")
# 项目语言-下拉框值
project_language_value = (By.XPATH, "//li[text()='{}']")

# 该仓库将是一个镜像(设置为镜像后，该项目为只读，不能进行push等相关操作)
mirror_type = (By.XPATH, "//input[@id='NewWorkForm_is_mirror']")

# 创建项目 按钮
create_project_button = (By.XPATH, "//span[text()='创建项目']/parent::button")
# 导入项目按钮
export_project_button = (By.XPATH, "//span[text()='导入项目']/parent::button")


# ------------------------------个人主页 操作---------------------------------------#
class UsersCenterPage(BasePage):
    """
    个人中心
    """

    def __init__(self, driver, login_name=None, host=None):
        self.host = host
        self.driver = driver
        self.url = self.host + "/" + str(login_name)

    def load(self):
        self.visit(self.url)
        return self

    def get_user(self):
        """
        获取用户的昵称/姓
        """
        return self.get_title(user_nickname)

    def click_new_icon(self):
        """# 点击 新建 图标"""
        self.hover(new_project_icon)
        time.sleep(2)
        return self

    def check_private_checkbox(self):
        """点击私有的checkbox"""
        self.hit(is_private_checkbox)
        return self

    def choose_project_type_language(self, **kwargs):
        """选择项目类别和语言"""
        time.sleep(2)
        # 选择项目类别
        self.hit(project_type_checkbox)
        self.hit(project_type_selectbox)
        pattern, locator = project_type_value
        self.hit((pattern, locator.format(kwargs.get("project_category"))))

        # 往下滑动
        js = "window.scrollTo(0, 100)"
        self.execute_js(js)

        time.sleep(5)
        # 选择项目语言
        self.hit(project_language_checkbox)
        self.hit(project_language_selectbox)
        pattern, locator = project_language_value
        self.hit((pattern, locator.format(kwargs.get("project_language"))))
        return self

    def new_project(self, **kwargs):
        """
        新建项目
        """
        time.sleep(2)
        # 点击 新建项目 按钮
        self.hit(create_project)
        # 输入项目名称
        self.input(project_name, kwargs.get("name"))
        # 输入项目标识
        self.input(depository_name, kwargs.get("repository_name"))
        # 输入项目简介
        self.input(project_desc, kwargs.get("desc"))

        # 选择gitignore
        self.hit(gitignore_checkbox)
        self.hit(gitignore_selectbox)
        pattern, locator = gitignore_value
        self.hit((pattern, locator.format(kwargs.get("ignore"))))

        # 选择开源许可证
        self.hit(certificate_checkbox)
        self.hit(certificate_selectbox)
        pattern, locator = certificate_value
        self.hit((pattern, locator.format(kwargs.get("license"))))

        if kwargs.get("private"):
            # 设置项目为私有
            self.check_private_checkbox()

        # 选择项目类别和语言
        self.choose_project_type_language(**kwargs)

        # 点击创建项目按钮
        self.hit(create_project_button)
        time.sleep(5)
        return self

    def export_project(self, **kwargs):
        """
        导入项目
        """
        time.sleep(2)
        # 点击 导入项目 按钮
        self.hit(export_project)
        # 输入镜像地址
        self.input(export_url, kwargs.get("mirror_url"))
        if kwargs.get("mirror_private"):
            self.hit(authorization_button)
            self.input(mirror_username, kwargs.get("mirror_user"))
            self.input(mirror_password, kwargs.get("mirror_pwd"))
        # 输入项目名称
        self.input(project_name, kwargs.get("name"))
        # 输入项目标识
        # self.input(depository_name, kwargs.get("repository_name"))

        # 输入项目简介
        self.input(project_desc, kwargs.get("desc"))

        if kwargs.get("private"):
            # 设置项目为私有
            self.check_private_checkbox()

        if kwargs.get("mirror_type"):
            # 该仓库将是一个镜像(设置为镜像后，该项目为只读，不能进行push等相关操作)
            self.hit(mirror_type)

        # 选择项目类别和语言
        self.choose_project_type_language(**kwargs)

        # 点击导入项目按钮
        self.hit(export_project_button)
        time.sleep(5)
        return self
