# -*- coding: utf-8 -*-
# @Time    : 2021/8/14 12:22
# @Author  : Flora.Chen
# @File    : handle_excel.py
# @Software: PyCharm
# @Desc: 使用openpyxl对excel进行读写操作
import os

import openpyxl
from loguru import logger


class HandleExcel:
    """
    从excel读取写入数据的类
    """

    def __init__(self, file):
        """
        初始化用例文件
        :param file: 文件绝对路径，如：D:\test\test.xlsx
        """
        self.file = file

    def create_excel(self, excel_name):
        """
        创建excel文件
        """
        # 创建文件对象
        wb = openpyxl.Workbook()
        path = os.path.join(self.file, excel_name)
        logger.debug(f"self.file：{self.file}")
        logger.debug(f"excel_name：{excel_name}")
        logger.debug(f"创建的excel路径：{path}")
        wb.save(path)
        self.file = path

    def read(self, sheet_name):
        """
        读取excel数据并返回
        :param sheet_name: 表单名称
        :return: 存在传入的表单, 返回表单数据，不存在则返回空
        """
        # 创建一个工作簿工作对象(excel文件已存在的情况)
        workbook = openpyxl.open(self.file)
        # 跟上面那句一个意思 workbook = openpyxl.load_workbook(self.file)

        # 获取excel当中所有的sheet，返回的是一个列表
        sheets = workbook.sheetnames
        # 获取sheet对象
        if sheet_name in sheets:
            sheet = workbook[sheet_name]
            all_values = list(sheet.values)
            header = all_values[0]
            data = []
            for i in all_values[1:]:
                data.append(dict(zip(header, i)))
            # 关闭excel
            workbook.close()
            return data
        else:
            # 关闭excel
            workbook.close()
            logger.error(f"表单： 【{sheet_name}】文件不存在")

    def write(self, row, column, data, sheet_name=None):
        """
        往excel写入数据
        :param sheet_name: 表单名称
        :param row: 要写入的行
        :param column: 要写入的列
        :param data: 要写入的数据
        :return: None
        """
        workbook = openpyxl.open(self.file)
        # 获取excel当中所有的sheet，返回的是一个列表
        sheets = workbook.sheetnames
        if sheet_name in sheets:
            sheet = workbook[sheet_name]
            logger.error(f"往表单【{sheet_name}】中写入数据")
        else:
            # 如果表单为空，就默认使用第一个表单
            sheet = workbook.active
            logger.error(f"表单【{sheet_name}】不存在，默认往第一个表单中写入数据")

        sheet.cell(row=row, column=column, value=data)
        # 更上面写法效果一样 sheet.cell(row=row, column=column).value = data

        # 保存并关闭文件
        workbook.save(self.file)
        workbook.close()
