# -*- coding: utf-8 -*-
# @Time    : 2021/8/14 12:13
# @Author  : Flora.Chen
# @File    : handle_time.py
# @Software: PyCharm
# @Desc: 时间计算的方法

from datetime import datetime, timedelta

now = datetime.now().strftime("%Y-%m-%d %H_%M_%S")


class AddTime:
    """
    计算时间加减的类
    """

    def __init__(self, start_time=now, time_unit="days", interval=1):
        if isinstance(start_time, str):
            start_time = datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S")
        self.start_time = start_time
        self.time_unit = time_unit
        self.interval = interval

    def add_time(self):
        """
        在指定时间基础上加减指定时间
        :return: 计算结果
        """
        if self.time_unit == "seconds":
            return self.add_seconds()
        elif self.time_unit == "hours":
            return self.add_hours()
        elif self.time_unit == "weeks":
            return self.add_weeks()
        elif self.time_unit == "minutes":
            return self.add_minutes()
        else:
            return self.add_day()

    def add_minutes(self):
        """
        在当前时间基础上加减指定分钟
        :return: 计算结果
        """
        return (self.start_time + timedelta(minutes=self.interval)).strftime("%Y-%m-%d %H:%M:%S")

    def add_hours(self):
        """
        在当前时间基础上加减指定小时
        :return: 计算结果
        """
        return (self.start_time + timedelta(hours=self.interval)).strftime("%Y-%m-%d %H:%M:%S")

    def add_day(self):
        """
        在当前时间基础上加减指定天
        :return: 计算结果
        """
        return (self.start_time + timedelta(days=self.interval)).strftime("%Y-%m-%d %H:%M:%S")

    def add_seconds(self):
        """
        在当前时间基础上加减指定秒
        :return: 计算结果
        """
        return (self.start_time + timedelta(seconds=self.interval)).strftime("%Y-%m-%d %H:%M:%S")

    def add_weeks(self):
        """
        在当前时间基础上加减指定周
        :return: 计算结果
        """
        return (self.start_time + timedelta(weeks=self.interval)).strftime("%Y-%m-%d %H:%M:%S")


def mistiming(start_time, end_time):
    """计算两个时间差"""
    stime = datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S")
    etime = datetime.strptime(end_time, "%Y-%m-%d %H:%M:%S")
    total_seconds = (etime - stime).total_seconds()
    if total_seconds < 0:
        total_seconds = (stime - etime).total_seconds()
    day = total_seconds // (60 * 60 * 24)
    rest_seconds = total_seconds % (60 * 60 * 24)
    hours = rest_seconds // (60 * 60)
    rest_seconds = rest_seconds % (60 * 60)
    mins = rest_seconds // 60
    seconds = rest_seconds % 60
    return f"{int(day)}天 {int(hours)}小时 {int(mins)}分钟 {int(seconds)}秒"
