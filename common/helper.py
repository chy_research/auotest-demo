# -*- coding: utf-8 -*-
# @Time    : 2021/8/14 12:17
# @Author  : Flora.Chen
# @File    : helper.py
# @Software: PyCharm
# @Desc: 封装一些小方法


import os
from faker import Faker
import urllib.request
from loguru import logger
import zipfile
import os


def get_newest_file(path):
    """获取目录中创建时间最新的文件名"""
    files = os.listdir(path)
    a = 0
    newest_file = ""
    for file in files:
        file_path = os.path.join(path, file)
        timestamp = os.path.getctime(file_path)
        if timestamp > a:
            a = timestamp
            newest_file = file_path
    return newest_file


def generate_characters(n):
    """随机生成指定长度的字符"""
    fk = Faker("zh_CN")
    res = fk.name()
    while True:
        if len(res) < n:
            res = res + fk.sentence()
        else:
            break
    return res[:n]


def create_dir(path):
    """创建目录"""
    if not os.path.exists(path):
        os.mkdir(path)
        logger.debug(f"成功创建目录: {path}")
    logger.debug(f"目录地址已存在: {path}")


def download_img(url, path):
    """
    通过Python自带的库urllib下载图片
    需要导入：import urllib.request
    :param url: url地址
    :param path: 图片保存的绝对路径
    """
    try:
        request = urllib.request.Request(url)
        response = urllib.request.urlopen(request)
        if response.getcode() == 200:
            logger.debug("请求状态码200， 开始下载图片......")
            with open(path, "wb") as fb:
                fb.write(response.read())
            return path
    except Exception as e:
        logger.debug("捕获到异常，下载图片失败")
        return f"{e}"


def zip_file(file_path: str, out_path: str):
    """
    压缩指定文件夹
    :param file_path: 目标文件夹路径
    :param out_path: 压缩文件保存路径+xxxx.zip
    :return: 无
    """
    # 如果传入的路径是一个目录才进行压缩操作
    if os.path.isdir(file_path):
        logger.debug("目标路径是一个目录，开始进行压缩......")
        # 写入
        zip = zipfile.ZipFile(out_path, "w", zipfile.ZIP_DEFLATED)
        for path, dirnames, filenames in os.walk(file_path):
            # 去掉目标跟路径，只对目标文件夹下边的文件及文件夹进行压缩
            fpath = path.replace(file_path, '')
            for filename in filenames:
                zip.write(
                    os.path.join(
                        path, filename), os.path.join(
                        fpath, filename))
        zip.close()
        logger.debug("压缩完成！")
    else:
        logger.error("目标路径不是一个目录，请检查！")


def unzip_file():
    pass


def delete_dir_file(file_path):
    """
    删除指定目录下的所有文件
    :param file_path: 目标文件夹路径 (存在多级路径的暂不支持)
    """
    paths = os.listdir(file_path)
    if paths:
        logger.debug("目标目录存在文件或目录，进行删除操作")
        for item in paths:
            path = os.path.join(file_path, item)
            # 如果目标路径是一个文件，使用os.remove删除
            if os.path.isfile(path):
                os.remove(path)
            # 如果目标路径是一个目录，使用os.rmdir删除
            if os.path.isdir(path):
                os.rmdir(path)
    else:
        logger.debug("目标目录不存在文件或目录，不需要删除")


if __name__ == '__main__':
    delete_dir_file(r"/home/flora/chy/auotest/report")
