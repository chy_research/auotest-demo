# -*- coding: utf-8 -*-
# @Time    : 2021/9/10 13:44
# @Author  : Flora.Chen
# @File    : download_img.py
# @Software: PyCharm
# @Desc: 下载图片

import urllib.request
from loguru import logger


def download_img(url, path):
    """
    通过Python自带的库urllib下载图片
    需要导入：import urllib.request
    :param url: url地址
    :param path: 图片保存的绝对路径
    """
    try:
        request = urllib.request.Request(url)
        response = urllib.request.urlopen(request)
        if response.getcode() == 200:
            logger.debug("请求状态码200， 开始下载图片......")
            with open(path, "wb") as fb:
                fb.write(response.read())
            return path
    except Exception as e:
        logger.debug("捕获到异常，下载图片失败")
        return f"{e}"
