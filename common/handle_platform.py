# -*- coding: utf-8 -*-
# @Time    : 2021/8/19 9:13
# @Author  : Flora.Chen
# @File    : handle_platform.py
# @Software: PyCharm
# @Desc: 跨平台的支持allure，用于生成allure测试报告

import platform


class HandlePlatform:
    """跨平台的支持allure, webdriver"""

    @property
    def allure(self):
        if platform.system() == "Windows":
            cmd = "allure.bat"
        else:
            cmd = "allure"
        return cmd

