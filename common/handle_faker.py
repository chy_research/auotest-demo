# -*- coding: utf-8 -*-
# @Time    : 2021/8/23 14:32
# @Author  : Flora.Chen
# @File    : handle_faker.py
# @Software: PyCharm
# @Desc: 使用faker模块造测试数据

from faker import Faker

# 生成中文的随机数据
z_fk = Faker(locale="zh_CN")
# 生成英文的随机数据
e_fk = Faker()

# 随机名字
z_fk_name = z_fk.name()
e_fk_name = e_fk.name()

# ----------------------------- 文本相关 ----------------------------------#
# 单个文本
z_fk_text = z_fk.text(max_nb_chars=200, ext_word_list=None)
e_fk_text = e_fk.text(max_nb_chars=200, ext_word_list=None)
# 多个句子
z_fk_sentences = z_fk.sentences(nb=3, ext_word_list=None)
e_fk_sentences = e_fk.sentences(nb=3, ext_word_list=None)
