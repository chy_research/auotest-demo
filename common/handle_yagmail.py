# -*- coding: utf-8 -*-
# @Time    : 2021/8/14 12:21
# @Author  : Flora.Chen
# @File    : handle_yagmail.py
# @Software: PyCharm
# @Desc: 通过第三方模块yagmail发送邮件

import yagmail
from loguru import logger
import os


class EmailServe:

    @staticmethod
    def send_email(setting: dict):
        """
        入参一个字典
        :param user: 发件人邮箱
        :param password: 邮箱授权码
        :param host: 发件人使用的邮箱服务 例如：smtp.163.com
        :param contents: 内容
        :param addressees: 收件人列表
        :param title: 邮件标题
        :param enclosures: 附件列表
        :return:
        """
        logger.debug("------------ 连接邮件服务器 ---------------")
        yag = yagmail.SMTP(
            setting['user'],
            setting['password'],
            setting['host'])
        logger.debug("------------ 开始发送邮件 ---------------")
        if os.path.exists(setting['enclosures']):
            yag.send(
                to=setting['addressees'],
                subject=setting['title'],
                contents=setting['contents'],
                attachments=setting['enclosures'])
        else:
            yag.send(
                to=setting['addressees'],
                subject=setting['title'],
                contents=setting['contents'])
        logger.debug("------------ 邮件发送完毕，关闭邮件服务器连接 ---------------")
        yag.close()


