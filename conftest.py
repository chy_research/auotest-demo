# -*- coding: utf-8 -*-
# @Time    : 2021/8/14 12:33
# @Author  : Flora.Chen
# @File    : draft_bk.py.py
# @Software: PyCharm
# @Desc:

import pytest
from loguru import logger
from config.settings import EnvConfig
from api.login_api import TrustieLogin
from api.project_api import ProjectApi


# ------------------------------------- START: 配置运行环境 ---------------------------------------#

def pytest_addoption(parser):
    """
    pytest_addoption 可以让用户注册一个自定义的命令行参数，方便用户将数据传递给 pytest；
    这个 Hook 方法一般和 内置 fixture pytestconfig 配合使用，pytest_addoption 注册命令行参数，pytestconfig 通过配置对象读取参数的值；
    :param parser:
    :return:
    """

    parser.addoption(
        # action="store" 默认，只存储参数的值，可以存储任何类型的值，此时 default 也可以是任何类型的值，而且命令行参数多次使用也只能生效一个，最后一个值覆盖之前的值；
        "--env", action="store",
        default="test",
        choices=["test", "live"],  # choices 只允许输入的值的范围
        help="通过自定义命令行参数-env设置当前运行的环境"
    )


# 从配置对象中读取自定义参数的值
@pytest.fixture(scope="session", autouse=True)
def env(request):
    env = request.config.getoption("--env")
    env_mapping = EnvConfig.env_mapping.value
    env = env_mapping.get(f"{env}")
    forge_env = env.get("forge")
    logger.debug(f"当前的执行环境：{forge_env}")
    return {"forge_env": forge_env}


@pytest.fixture(scope="session")
def get_user(request):
    env = request.config.getoption("--env")
    users_mapping = EnvConfig.users_mapping.value
    users = users_mapping.get(f"{env}")
    return users


# ------------------------------------- END: 配置运行环境 ---------------------------------------#

@pytest.fixture(scope="session")
def get_cookies(env, get_user):
    login = TrustieLogin(host=env)
    return login.login_api(user=get_user.get("user"), pwd=get_user.get("pwd")).cookies

