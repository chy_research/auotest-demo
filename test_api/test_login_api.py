# -*- coding: utf-8 -*-
# @Time    : 2021/8/14 23:46
# @Author  : Flora.Chen
# @File    : test_login_ui.py
# @Software: PyCharm
# @Desc:
import os.path
import allure
import pytest
from config.settings import API_DATA
from common.handle_yaml import HandleYaml
from common.baseapi import BaseApi
from loguru import logger


@pytest.mark.api
@pytest.mark.forge
@allure.feature("登录模块-API")
class TestLoginApi:
    """
    登录的API测试用例
    """
    cases = HandleYaml(os.path.join(API_DATA, "login_data.yaml")).read()["case"]

    @allure.story("登录接口的API测试用例")
    @pytest.mark.parametrize("case", cases)
    def test_login(self, case, env):
        """
        正确用户名和密码登录成功
        """
        http = BaseApi()
        host = env.get("forge_env")
        response = http.send_request(url=host + case["url"], method=case["method"], json=case["json"], verify=False)
        logger.debug(response.json())
        result = response.json()

        assert result["login"] == case["expected"]

