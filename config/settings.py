# -*- coding: utf-8 -*-
# @Time    : 2021/8/14 22:50
# @Author  : Flora.Chen
# @File    : settings.py
# @Software: PyCharm
# @Desc: 配置文件
import enum
import os
import platform

# 项目根目录
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# 通用模块目录
COMMON_DIR = os.path.join(BASE_DIR, "common")

# 第三方库目录
LIB_DIR = os.path.join(BASE_DIR, "lib")

# 配置模块目录
CONF_DIR = os.path.join(BASE_DIR, "config")

# 数据模块目录
DATA_DIR = os.path.join(BASE_DIR, "data")

# API数据模块目录
API_DATA = os.path.join(DATA_DIR, "api_data")

# UI数据模块目录
UI_DATA = os.path.join(DATA_DIR, "ui_data")

# 日志保存目录
LOG_DIR = os.path.join(BASE_DIR, "log")
if not os.path.exists(LOG_DIR):
    os.mkdir(LOG_DIR)

# 图片保存目录
IMG_DIR = os.path.join(BASE_DIR, "image")
if not os.path.exists(IMG_DIR):
    os.mkdir(IMG_DIR)

# 报告保存目录
REPORT_DIR = os.path.join(BASE_DIR, "report")
if not os.path.exists(REPORT_DIR):
    os.mkdir(REPORT_DIR)

# API测试用例模块
API_CASE_DIR = os.path.join(BASE_DIR, "test_api")

# API测试用例模块
UI_CASE_DIR = os.path.join(BASE_DIR, "test_ui")


class EnvConfig(enum.Enum):
    """环境配置信息"""
    env_mapping = {
        "test": {
            "forge": "https://test*****.trustie.net"
        },
        "live": {
            "forge": "https://*****.trustie.net"
        },
    }

    # 不同环境对应的用户名密码
    users_mapping = {
        "test": {"user": "*****.", "pwd": "*****.", "nickname": "*****."},
        "live": {"user": "*****.", "pwd": "*****.@", "nickname": "*****."}
    }

    # 数据库配置
    db_mapping = {

    }


class RunConfig:
    """
    运行测试配置
    """
    # 配置浏览器驱动类型(chrome/firefox/chrome-headless/firefox-headless)。
    driver_type = "chrome-headless"
    # driver_type = "chrome"

    # 失败重跑次数
    rerun = "0"

    # 当达到最大失败数，停止执行
    max_fail = "10"

    # 浏览器驱动（不需要修改）
    driver = None

    # 浏览器驱动放置的位置
    if platform.system() == "Linux":
        if "chrome" in driver_type:
            driver_path = "/usr/bin/chromedriver"
        if "firefox" in driver_type:
            driver_path = ""
    else:
        if "chrome" in driver_type:
            driver_path = r"D:/Program Files/python39/chromedriver.exe"
        if "firefox" in driver_type:
            driver_path = ""


# 基准的请求头信息
headers = {
    "Accept-Encoding": "gzip, deflate",
    "Accept-Language": "zh-CN,zh;q=0.9",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36"
}

# 发送邮件的相关配置信息
email = {
    "user": "*****.@*****..com.cn",  # 发件人邮箱
    "password": "*****",  # 发件人邮箱授权码
    "host": "smtp.qiye.aliyun.com",
    "contents": ['<h1 class="title" style="text-align: center;color: #bd2c00">API & UI 自动化测试报告</h1>',
                 '<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;各位同事，大家好!</h3>',
                 '<h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本轮自动化测试已完成！可以通过下载附件查看测试报告的详细信息！</h4>',
                 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;查看测试报告的方法：<br/>"
                 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;下载附件：est_report.zip(自动化测试报告)<br/>"
                 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本地解压test_report.zip(自动化测试报告)<br/>",
                 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;请使用已安装Live Server 插件的VsCode，打开解压目录下的index.html查看报告<br/>",
                 "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"]
    ,
    "addressees": ["*****.@*****..com.cn"],  # 收件人邮箱
    "title": "自动化测试报告",
    "enclosures": os.path.join(REPORT_DIR, "autotest_report.zip")  # 附件
}
