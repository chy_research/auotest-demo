# -*- coding: utf-8 -*-
# @Time    : 2021/8/23 14:25
# @Author  : Flora.Chen
# @File    : new_project_data.py
# @Software: PyCharm
# @Desc:

from common.handle_faker import *
import random

new_project = [
    {
        "title": "新建公有项目",
        "owner": "",
        "name": "Auto Test " + e_fk.name(),
        "desc": z_fk_text,
        "repository_name": e_fk.name().replace(" ", ""),
        "project_category": random.choice(["机器学习", "大数据", "深度学习", "人工智能", "量子计算", "智慧医疗", "自动驾驶", "其他"]),
        "project_language": random.choice(["C", "Ruby", "C#", "HTML", "C++"]),
        "ignore": random.choice(["Ada", "Actionscript", "Ansible", "Android", "Agda"]),
        "license": random.choice(["0BSD", "AAL", "AFL-1.1", "389-exception"]),
        "private": False,
        "expected": ""
    },
    {
        "title": "新建私有项目",
        "owner": "",
        "name": "Auto Test " + e_fk.name(),
        "desc": z_fk_text,
        "repository_name": e_fk.name().replace(" ", ""),
        "project_category": random.choice(["机器学习", "大数据", "深度学习", "人工智能", "量子计算", "智慧医疗", "自动驾驶", "其他"]),
        "project_language": random.choice(["C", "Ruby", "C#", "HTML", "C++"]),
        "ignore": random.choice(["Ada", "Actionscript", "Ansible", "Android", "Agda"]),
        "license": random.choice(["0BSD", "AAL", "AFL-1.1", "389-exception"]),
        "private": True,
        "expected": ""
    }

]

export_project = [
    {
        "title": "导入项目（公有）-原项目是公有项目",
        "owner": "",
        "mirror_url": "https://gitee.com/paddlepaddle/Parakeet.git",
        "mirror_private": False,
        "name": "Auto Test " + e_fk.name(),
        "desc": z_fk_text,
        "repository_name": "Parakeet",
        "project_category": random.choice(["机器学习", "大数据", "深度学习", "人工智能", "量子计算", "智慧医疗", "自动驾驶", "其他"]),
        "project_language": random.choice(["C", "Ruby", "C#", "HTML", "C++"]),
        "ignore": random.choice(["Ada", "Actionscript", "Ansible", "Android", "Agda"]),
        "private": False,
        "expected": "",
        "mirror_type": False
    },
    {
        "title": "导入项目（私有）-原项目是公有项目",
        "owner": "",
        "mirror_url": "https://gitee.com/chinasoft4_ohos/NewQuickAction.git",
        "mirror_private": False,
        "name": "Auto Test " + e_fk.name(),
        "desc": z_fk_text,
        "repository_name": "NewQuickAction",
        "project_category": random.choice(["机器学习", "大数据", "深度学习", "人工智能", "量子计算", "智慧医疗", "自动驾驶", "其他"]),
        "project_language": random.choice(["C", "Ruby", "C#", "HTML", "C++"]),
        "ignore": random.choice(["Ada", "Actionscript", "Ansible", "Android", "Agda"]),
        "private": True,
        "expected": "",
        "mirror_type": True
    },
    {
        "title": "导入项目（私有）-原项目是私有项目",
        "owner": "",
        "mirror_url": "https://gitee.com/florachy/notification-system.git",
        "mirror_user": "*****",
        "mirror_pwd": "*****",
        "mirror_private": True,
        "name": "Auto Test " + e_fk.name(),
        "desc": z_fk_text,
        "repository_name": "notification-system",
        "project_category": random.choice(["机器学习", "大数据", "深度学习", "人工智能", "量子计算", "智慧医疗", "自动驾驶", "其他"]),
        "project_language": random.choice(["C", "Ruby", "C#", "HTML", "C++"]),
        "ignore": random.choice(["Ada", "Actionscript", "Ansible", "Android", "Agda"]),
        "private": True,
        "expected": "",
        "mirror_type": True
    },
    {
        "title": "导入项目（公有）-原项目是私有项目",
        "owner": "",
        "mirror_url": "https://gitee.com/florachy/finger-change.git",
        "mirror_user": "*****",
        "mirror_pwd": "*****",
        "mirror_private": True,
        "name": "Auto Test " + e_fk.name(),
        "desc": z_fk_text,
        "repository_name": "finger-change",
        "project_category": random.choice(["机器学习", "大数据", "深度学习", "人工智能", "量子计算", "智慧医疗", "自动驾驶", "其他"]),
        "project_language": random.choice(["C", "Ruby", "C#", "HTML", "C++"]),
        "ignore": random.choice(["Ada", "Actionscript", "Ansible", "Android", "Agda"]),
        "private": False,
        "expected": "",
        "mirror_type": False
    }

]
