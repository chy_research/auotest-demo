# -*- coding: utf-8 -*-
# @Time    : 2021/8/14 14:22
# @Author  : Flora.Chen
# @File    : login_data.py
# @Software: PyCharm
# @Desc:


# 登录成功
success = [
    {"title": "正确的用户名和密码登录成功", "user": "{}", "pwd": "{}", "expected": "{}"}
]

# 登录失败-错误提示是输入框下红色提示
failed_error_message = [
    {"title": "用户名为空，登录失败，有错误提示：用户名不能为空", "user": "", "pwd": "{}", "expected": ["用户名不能为空"]}

]

# 登录失败，错误提示是弹窗
failed_error_pop = [
    {"title": "用户名错误，登录失败，提示错误的账号或密码：", "user": "18774970061111", "pwd": "", "expected": ["错误的账号或密码"]},
    {"title": "用户名正确，密码第一次错误，提示：你已经输错密码1次，还剩余4次机会", "user": "{}", "pwd": "1111111111",
     "expected": ["你已经输错密码1次，还剩余4次机会"]},
    {"title": "用户名正确，密码第二次错误，提示：你已经输错密码2次，还剩余3次机会", "user": "{}", "pwd": "11111222222",
     "expected": ["你已经输错密码2次，还剩余3次机会"]},
    {"title": "用户名正确，密码第三次错误，提示：你已经输错密码3次，还剩余2次机会", "user": "{}", "pwd": "1111333333",
     "expected": ["你已经输错密码3次，还剩余2次机会"]},
    # {"title": "用户名正确，密码第四次错误，提示：你已经输错密码4次，还剩余1次机会", "user": "{}", "pwd": "11115555555",
    #  "expected": ["你已经输错密码4次，还剩余1次机会"]},
    # {"title": "用户名正确，密码第五次错误，提示：登录密码出错已达上限，账号已被锁定, 请60分钟后重新登录或找回密码", "user": "{}", "pwd": "11115555555",
    #  "expected": ["登录密码出错已达上限，账号已被锁定, 请60分钟后重新登录或找回密码"]}

]

# 登录失败，密码为空不能点击登录
failed_password_empty = [
    {"title": "用户名正确， 密码为空，登录按钮无法点击", "user": "{}", "pwd": "", "expected": "bluebutton"}
]
